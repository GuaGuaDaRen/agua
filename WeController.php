<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;//继承控制器
use frontend\models\Reg;
use yii\data\Pagination;//调用分页
use yii\db\ActiveRecord;//AR(手册----活动记录) 操作数据库进行增删改删
use DfaFilter\SensitiveHelper;
use yii\web\Request;

/**
 * 仿微信开发 ``````````服务器端
 */
class WeController extends Controller
{

    public $enableCsrfValidation = false;
    //注册
    public function actionIndex()
    {
        //添加
        if (Yii::$app->request->isPost) {

            $post=Yii::$app->request->post();
            //自动生成appid
            $str='qwertyuioplkjhgfdsazxcvbnm0123456789';
            $a='';
            for ($i=0;$i<8;$i++) {
                $a.=mt_rand(0,strlen($str)-1);
            }
            $post['r_appid']=$a;
            //自动生成secret
            $s='';
            for ($i=0;$i<8;$i++) {
                $s.=mt_rand(0,strlen($str)-1);
            }
            $post['r_secret']=md5($s);
            $model=new Reg();
            $model->load(['Reg'=>$post]);
            $res=$model->save();
            if ($res) {
                 echo "<script>alert('注册成功!')</script>";
                 $this->redirect(['/we/login']);
            }

        } else {
            echo "<script>alert('注册失败!')</script>";
            return $this->render('reg');
        }

    }

    //登录
    public function actionLogin()
    {

        if(Yii::$app->request->isPost)
        {
            $post=Yii::$app->request->post();
            $models=new Reg();
            $res=$models->find()->where(['r_name'=>$post['r_name'],'r_pwd'=>$post['r_pwd']])->asArray()->one();
            if ($res) {
                //存session
                $session = Yii::$app->session;
                $session->set('r_id',$res['r_id']);
                $session->set('r_name',$res['r_name']);

                echo "<script>alert('登录成功!')</script>";
                $this->redirect(['we/infos']);

            } else {
                echo "no!";
            }

        } else {

            echo "<script>alert('登录失败!')</script>";
            return $this->render('login');
        }

    }

    //设置
    public function actionInfos()
    {
        if(Yii::$app->request->isPost)
        {
            //取session
            $session = Yii::$app->session;
            $r_id=$session->get('r_id');
            $post=Yii::$app->request->post();
            //设置安全域名
            $sql="UPDATE reg set r_url = '$post[r_url]' WHERE `r_id`='$r_id'";
            $res = Yii::$app->db->createCommand($sql)->execute();
            if ($res) {
                return $this->redirect(['we/infos']);
            }


        } else {
            //取session
            $session = Yii::$app->session;
            $r_id=$session->get('r_id');
            $models=new Reg();
            //获取当前用户的appid 和 appsecret
            $arr=$models->find()->where(['r_id'=>$r_id])->asArray()->one();

            return $this->render('urlss',['arr'=>$arr]);
        }


    }

    //接收请求
    public function actionGettooken()
    {
         if(Yii::$app->request->isGet) {

            //接收被调用传过来的数据
            $get=Yii::$app->request->get();
            $appid=$get['appid'];
            $appsecret=$get['appsecret'];
            $models=new Reg();
            //查询当前 的appid 和 appsecret
            $arr=$models->find()->where(['r_appid'=>$appid,'r_secret'=>$appsecret])->asArray()->one();

            if (empty($arr)) {
                echo "40003";

            } else {
               if ($_SERVER['HTTP_REFERER'] != 'http://'.$arr['r_url'].'/' ) {
                        echo '4002';
                    } else {

                        $token = md5(mt_rand(0000,9999));
                        $sql="insert into reg(`r_token`) value('".$token."')";
                        $res = Yii::$app->db->createCommand($sql)->execute();
                        echo json_encode($token);
                    }
            }


         }


    }


}
/**
// （1）题目要求：
// 项目名称：仿微信公众号接口开发
// 项目需求：程序需配置两个域名，一个为微信服务器端，另一个为程序开发端，微信服务器端可以登录注册用户，注册用户时自动生成appID和appsecret（32位随机字符串），登陆后可设置接口安全域名
// 程序开发端把appid，appsecret拼接到url链接后面，通过GET请求到微信服务器端，需微信服务器端确认数据库里的该appid对应的appsecret正确，并请求域名与接口安全域名一致，查询后返回json数据，成功则返回access_token，access_token重复获取将导致上次获取的access_token失效。
 公众号每次调用接口时ddddddddddddd，可能获得正确或错误的返回码，开发者可以根据返回码信息调试接口，排查错误，返回码不能少于四种*/